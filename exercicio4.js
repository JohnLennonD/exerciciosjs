/* Vocês receberão um arquivo com um array de objetos representando
deuses do jogo Smite. Usando os métodos aprendidos em aula, faça os
seguintes exercícios:

● Q1. Imprima o nome e a quantidade de features de todos os deuses usando uma única linha de código.
● Q2. Imprima todos os deuses que possuem o papel de "Mid"
● Q3. Organize a lista pelo panteão do deus.
● Q4. Faça um código que retorne um novo array com o nome de cada deus e entre parênteses, a sua classe.
Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...] 
*/

import gods from "./arquivo_exercicio_4.js"

function funcQ1() {
    for (let god of gods) { console.log(god.name, god.features.length)}
}

function funcQ2() {
    for (let god of gods) {
        if (god.roles.includes('Mid')) {
            console.log(god)
        }
    }
}

function funcQ3() {
    gods.sort(function (a, b) {
        if (a.pantheon < b.pantheon) {
          return -1;
        }
        if (a.pantheon > b.pantheon) {
          return 1;
        }
        return 0;
    });

    console.log(gods)
}

function funcQ4() {
    const Ngods = []
    for (let god of gods) {
        Ngods.push(`${god.name} (${god.class})`)
    }
    console.log(Ngods)
}

funcQ1()
funcQ2()
funcQ3()
funcQ4()