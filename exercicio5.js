/* Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada.
*/

function testNumber() {
    let numbers = [17, 9, 25, 32, 15]

    for (let number of numbers) {
        if (number % 3 == 0 && number % 5 == 0) {
            console.log("fizzbuzz")
        }else if(number % 3 == 0 ) {
            console.log('fizz')
        }else if(number % 5 == 0) {
            console.log('buzz')
        }else{
            console.log('nada')
        }
    }
}

testNumber()