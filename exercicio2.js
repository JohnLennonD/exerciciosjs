/* . Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
calcular e escrever o valor correspondente em graus Celsius (baseado na
fórmula abaixo):

𝐶/5 = ( 𝑓 − 32)/9
*/

var fahr = prompt("Insira a temperatura em graus Fahrenheit: ")

function convertToFarh(fahr) {
    
    fahr = parseFloat(fahr)
    let cels = 5 * ((fahr - 32)/9)
    return cels.toFixed(2)

}

res = convertToFarh(fahr)
console.log(res)