/* Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
média aritmética dessas notas for maior ou igual que 6 imprima
“Aprovado”, caso contrário “Reprovado”.
*/

function mediaArit () {
    var notas = [5, 7, 8]
    var media = 0
    var qntNot = notas.length

    for (let nota of notas) {
        media += nota
    }

    mediaF = media/ qntNot
    return mediaF.toFixed(2)
}

console.log(mediaArit())
