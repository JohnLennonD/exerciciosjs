/* Implemente um algoritmo que pegue duas matrizes (array de arrays) e realize sua multiplicação. Lembrando que para realizar a multiplicação dessas matrizes o número de colunas da primeira matriz tem que ser igual ao número de linhas da segunda matriz. (2x2)
*/

// Resultado [[-4, 12], [-1, -10]]

function resolMatriz () {
    MA = [[4, 0], [-1, -1]]
    MB = [[-1, 3], [2, 7]]

    numRowMA = MA.length, numColMA = MA[0].length
    numRowMB = MB.length, numColMB = MB[0].length
    console.log (`Matriz A: ${numRowMA}x${numColMA} e Matriz B: ${numRowMB}x${numColMB}`)
    

    if ( numColMA == numRowMB) {
        console.log("A matriz é válida")
        M = new Array(numRowMA)

        for (var i = 0; i < numRowMA; i++) {
            M[i] = new Array(numColMB)
            for (var j = 0; j < numColMB; j++) {
                M[i][j] = 0
                console.log(`Na posição da Matriz M de linha:${i} e coluna:${j} recebe: `)
                for (var k = 0; k < numColMA; k++) {
                    let valor = MA[i][k] * MB[k][j]
                    console.log(`${MA[i][k]} * ${MB[k][j]} = ${valor}`)
                    
                    M[i][j] += MA[i][k] * MB[k][j]

                    // M.push([i][j]) += MA[i][k] * MB[k][j] 
                }
                console.log(`M${i}x${j} recebe: ${M[i][j]}`)
            }
        }
        return M
    }else {
        console.log("A matriz é inválida")
    }
}

res = resolMatriz()
console.log(res)